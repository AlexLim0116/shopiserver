  
const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/', (req, res, next) => {
	return res.send("This is server.");
});
router.post('/', (req, res, next) => {
	console.log(req.body);
	let personInfo = req.body;


	if (!personInfo.email || !personInfo.username || !personInfo.password || !personInfo.passwordConf) {
		res.send();
	} else {
		if (personInfo.password == personInfo.passwordConf) {

			User.findOne({ email: personInfo.email }, (err, data) => {
				if (!data) {
					let c;
					User.findOne({}, (err, data) => {

						if (data) {
							console.log("if");
							c = data.unique_id + 1;
						} else {
							c = 1;
						}

						let newPerson = new User({
							unique_id: c,
							email: personInfo.email,
							username: personInfo.username,
							password: personInfo.password,
							passwordConf: personInfo.passwordConf
						});

						newPerson.save((err, Person) => {
							if (err)
								console.log(err);
							else
								console.log('Success');
						});

					}).sort({ _id: -1 }).limit(1);
					res.send({ "Success": "You are regestered,You can login now." });
				} else {
					res.send({ "Success": "Email is already used." });
				}

			});
		} else {
			res.send({ "Success": "password is not matched" });
		}
	}
});

router.post('/login', (req, res, next) => {
	//console.log(req.body);
	User.findOne({ email: req.body.email }, (err, data) => {
		if (data) {
            data.comparePassword(req.body.password, function(err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    //console.log("Done Login");
                    req.session.userId = data.unique_id;
                    //console.log(req.session.userId);
                    res.send({ "result": true,"msg": "Success!" });

                } else {
                    res.send({ "result": false,"msg": "Wrong password!" });
                }
            });
			
		} else {
			res.send({ "result": false,"msg": "This Email Is not regestered!" });
		}
	});
});

router.post('/register', (req, res, next) => {
	//console.log(req.body);
	User.findOne({ email: req.body.email }, (err, data) => {
		if (data) {
            res.send({ "result": false, "msg": "The user already exist." });
		} else {
            const user = new User(req.body);
            user.save((err, Person) => {
                if (err)
                    res.send({ "result": false, "msg": err });
                else
                    res.send({ "result": true, "msg": "Successfully created." });
            });
		}
	});
});

router.get('/logout', (req, res, next) => {
	console.log("logout")
	if (req.session) {
		// delete session object
		req.session.destroy((err) => {
			if (err) {
				return next(err);
			} else {
				return res.redirect('/');
			}
		});
	}
});


module.exports = router;
